import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import cv2
import requests
import time
import base64

from collections import defaultdict
from io import StringIO, BytesIO
from matplotlib import pyplot as plt
from PIL import Image
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util
from addresses import video_feed_address, gotify_notification_server_address, gotify_notification_server_port, gotify_app_token

# Define the video stream
print("Getting video feed from this URL:")
print(video_feed_address)
try:
    cap = cv2.VideoCapture(video_feed_address)
except:
    print("There's no access to the IP camera. Check the addresses and the connectivity. Retry in 20 seconds")
    time.sleep(20)

# Example: cap = cv2.VideoCapture("http://158.58.130.148/mjpg/video.mjpg")
# What model to download.
# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_FROZEN_GRAPH = 'trained-inference-graphs/output_inference_graph_v1.pb/frozen_inference_graph.pb'
# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('annotations', 'label_map.pbtxt')

# Number of classes to detect
NUM_CLASSES = 5

# Download Model
# Load a (frozen) Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.compat.v1.GraphDef()
    with tf.io.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')


# Loading label map
# Label maps map indices to category names, so that when our convolution network predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility functions, but anything that returns a dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(
    label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


# Helper code
def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)


# Detection
gotify_complete_url = gotify_notification_server_address + ":" + gotify_notification_server_port + "/message?token=" + gotify_app_token
# myobj={"title":"Weed detected!","message":"Weed found (Daisy flower)", "priority":"5"}
# Example: requests.post("http://158.42.154.190:5001/message?token=AhuGIr3wViyIq21", data = myobj, timeout=2.50)
threshold = 0.5 # in order to get higher percentages you need to lower this number; usually at 0.01 you get 100% predicted objects
output_width = 320
with detection_graph.as_default():
    with tf.compat.v1.Session(graph=detection_graph) as sess:
        while True:
            #Try-catch in case there is no connection to the camera the script keeps trying
            try:
            # Read frame from camera
            ret, image_np = cap.read()
            # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
            image_np_expanded = np.expand_dims(image_np, axis=0)
            # Extract image tensor
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            # Extract detection boxes
            boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            # Extract detection scores
            scores = detection_graph.get_tensor_by_name('detection_scores:0')
            # Extract detection classes
            classes = detection_graph.get_tensor_by_name('detection_classes:0')
            # Extract number of detectionsd
            num_detections = detection_graph.get_tensor_by_name(
                'num_detections:0')
            # Actual detection.
            (boxes, scores, classes, num_detections) = sess.run(
                [boxes, scores, classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

            objects = {}
            for index, value in enumerate(classes[0]):
                object_dict = {}
                if scores[0, index] > threshold:
                    object_dict[(category_index.get(value)).get('name')] = scores[0, index]
                    objects.update(object_dict)
            print(objects)
            if 'mar' in objects:
                vis_util.visualize_boxes_and_labels_on_image_array(image_np,np.squeeze(boxes),np.squeeze(classes).astype(np.int32),np.squeeze(scores),category_index,use_normalized_coordinates=True,line_thickness=8)
                # Transforming image from numpy array. Reescale for better output in the phone
                scale_percent = int(output_width * 100/image_np.shape[1])
                output_height = int(image_np.shape[0] * scale_percent / 100)
                buf = BytesIO()
                plt.imsave(buf, cv2.resize(image_np, dsize=(output_width, output_height), interpolation=cv2.INTER_CUBIC), format='png')
                image_data = base64.b64encode(buf.getvalue()).decode('utf-8')
                encoding = "data:image/png;base64," + image_data
                print("Sending notification")
                requests.post(gotify_complete_url,json={'extras':{'client::display':{'contentType': 'text/markdown'}},'title':'Test!','message':f'![]({encoding})','priority':5})
                # Send notification if weed is found.
                print("Unwanted flower was just found. Service will restart in 10 seconds")
                time.sleep(10)
                cap = 0
                cap = cv2.VideoCapture(video_feed_address)

        except:
            print("There's no access to the IP camera. Check the addresses and the connectivity. Retry in 5 seconds")
            time.sleep(5)
            cap = cv2.VideoCapture(video_feed_address)
