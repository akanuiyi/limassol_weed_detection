# Delivered Content

This demo is based on a current trend on architecture in which cellular networks and machine learning techniques are applied to detect and eliminate unwanted weeds in crop fields. This demo consists of two main services: a tensorflow-based service for training a neural network and perform the detection of the weeds (receiving real-time video feed) and a service for notifying its presence in mobile devices (substituting the action of a robot).

Components:

- Sim-based video camera (4G, since 5G are not available yet). Online video feeds can be used for testing.
- Mobile phone for receiving the notifications and measuring data. Gotify APP installed from Android Play.
- Limassol 5G Hotspot.

# Steps to setup the demo

## Folder structure

Three services are required to perform the demo: (1) a tensorflow service with the object detection API included, (2) a gotify service as a notification server and (3) a nginx reverse proxy needed to deploy the previous serice. The Tensorflow models (including object detection API) are atached in modules. The folder structure is be the following:

```
Limassol_Weed_Detection
│   addresses.py
│   docker-compose.yml
│   nginx.conf
│   README.md
│
└───Gotify:
│   │   docker-compose.yml
│   └───gotify_data
│
└───Object_detection
    │   Dockerfile
    │   Dockerfile2
    │   Dockerfile3
    │   docker-compose.yml
    └───scripts
    │   │   docker-compose.yml
    │   └───bin
    └───Limassol_Tensorflow
        └───models
        └───scripts
        └───workspace
            └───training_demo
```

The main files/folders are the following:

- `addresses.py`. File that allows changing the url of the camera's video feed as well as the direction of the gotify server.
- `docker-compose.yml`. Entrypoint of the demo, where the services are instantiated and all the necessary volumes, ports, dependencies and variables are defined. The other docker-compose files are not intended for the demo.
- `nginx.conf`. Configuration file for the nginx reverse proxy. No certs are currently used.
- `README.md` (this file).
- `Gotify`. Folder that includes the database where the notifications and the needed data for the server are stored. It has a docker-compose to deploy just this service (not the demo).
- `Object_detection`. Folder that includes the Tensorflow offical and research models, including the object detection API. The workspace, which work mainnly with this API, contains the traing and the detection functions along with all the necessary files. The behaviour of the detection service (demo) is depicted in the Dockerfile. Dockerfile2 and 3 are prepared for previous training purposes (see next sections).

## Run the demo

The demo just requires of a host with docker and docker-compose installed and running to be deployed. No additional dependencies have to be installed.

- Update the addresses of the video feed and the notifications server: `sudo nano addresses.py`
- Start the service: `sudo docker-compose up`
- Stop the service if it was on: `sudo docker-compose down`

The notifications can be accessed via the Gotify android app, which can be downloaded from Google Play (using http://<host_ip_address>:5001), and via the web browser at http://localhost:5001/. The generic user/password is 5genesis/5g3n3s1s. Once the model is trained and a camera connected, a notification will be send when unwanted weed is detected (currently, for demo purposes, it is making use of a video feed from the camera of a mobile phone, notifications are being sent when daisy flowers are found, and then the detection service is paused for 10 seconds).

### Camera options

The idea is make use of real cameras, ideally one embedded within a robot specialized in spraying herbicide. Still, for demos purposes, a normal IP camera of even the camera of a mobile phone can be used. In any case, the http or rstp adress of the device should be indicated in the addresses.py file.

As a suggestion, the mobile app `IP Webcam`, which can be downloaded from Google store, can be used for sending video feed in real time with Android devices, using either WiFi or cellular network (which an address that should be reachable for the edge/core funcions).

## Training the model

Before detecting, the model has to be trained. To this end, a script has been prepared to automatize the process (in `Object_detection/scripts/bin/start_training`). The script runs the functions shown in the official Object Detection API of Tensorflow, being the version 1.14 the one installed (https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/1.14.2/).

### Previous steps

Two steps have to be performed before executing this file and performing the training (can be done without having docker up in this point). Sthe official documentation of the API for more information.

- Annotate images. To annotate images the `labelImg` package can be used. Then, one can select boxes indicating the label. Ideally, at least 100 images per target object. Then, distribute ~90% in `training_demo\images\train folder` and the rest in `training_demo\images\test`.
- Create labelmap. This is a file with the extention .pbtxt that should be placed inside the `training_demo\annotations`. Assuming a dataset of two classes (dogs and cats), the labelmap should be something such as:

```
item {
    id: 1
    name: 'cat'
}

item {
    id: 2
    name: 'dog'
}
```

### Training script

The Dockerfile related to Tensorflow is prepared to execute the funtion related to the detection of the unwanted weeds. To perform the training, we can try to different ways. The first one is to run the demo's compose file (see "Run the demo") and then:

- In a new terminal, find the image id with `sudo docker ps`.
- Go into the container with `sudo docker exec -it <container_id> /bin/sh`.
- Navigate inside the container to the scripts/bin folder (`cd bin`, since when going into it should be in scrpts folder) and run `./start_training`.
- Once enough steps have been done (it depends on the used model and the loss value being returned), kill the process (Ctrl+C) and the script will automatically export the inference graph.

The second way implies making use of the additional Dockerfiles, which are identical to the one for detecting but with CMD prepared for training and exporting the inference graph, respectively:

- Go to the folder `Object_detection` and execute `sudo docker-compose up` for trainig, killing it when the losses are suitable (otherwise it would overtrain the model).
- To export the inference graph, execute `sudo docker-compose up` in `Object_detection/scripts` folder.

### Development vs production

Currently, volumes are used for both detecting and training, as new files are being generated constantly and we need them updated. Still, the idea is to develop a docker image with all the functions and needed libraries included once the final training for the demo is finished, without the need of external volumes, since for detecting purposes no new files are generated.

### Use of GPU graphic cards

Since no GPU is available in the hotspot, files have been prepared without its suport. However, its use would increase the speed of training and detection around an 80-90%. In case that GPU is utilized, the Object_detection services (training, detection and inference graph export) cannot be run in docker-compose files, since they do not support NVIDIA CUDA yet. Hence, docker run commands should be used after building the images (Dockerfile, Dockerfile2 and/or Dockerfile3), for instance: `sudo docker run -it --gpus all -v pwd/Limassol_Tensorflow:/Limassol_Tensorflow <image_id>` (pwd with inversed accents, reserved in md files). To make it work, NVIDIA driver (currently, 450.66, compatible with CUDA 10.0 which is needed for Tensorflow version 1.14 and NVIDIA Container Toolkit) must be installed in host.

# Remote OpenStack connection details (IPs AND CREDENTIALS TO BE UPDATED!)

To be able to connect to the OpenStack, they should provide you the credentials for the VPN
Once you have the VPN established you can access OSM dashboard on http://10.10.5.102 (user: admin, pass: admin) or ssh (user: Ubuntu, passphrase: osmpass ) using attached private key “id_rsa”.

Also, the OpenStack dashboard: http://10.10.5.2/horizon/auth/login/ (User: admin, Pass: 5genesislim@@)

## Troubleshooting

If any problem arises, please contact me by mail: alforlea@upv.es
